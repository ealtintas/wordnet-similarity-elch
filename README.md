Please use the reference below:

```
  @inproceedings{altintas-etal-2006-new,
      title = "A new semantic similarity measure evaluated in word sense disambiguation",
      author = "Altintas, Ergin  and
        Karsligil, Elif  and
        Coskun, Vedat",
      booktitle = "Proceedings of the 15th Nordic Conference of Computational Linguistics ({NODALIDA} 2005)",
      month = may,
      year = "2006",
      address = "Joensuu, Finland",
      publisher = "University of Joensuu, Finland",
      url = "https://www.aclweb.org/anthology/W05-1702", 
      pages = "8--11",
  }
```
